import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JSONResponse } from '../../model/JSONResponse';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {
  url:string= "http://www.mocky.io/v2/5ea15752320000680094b446";
  constructor(public http: HttpClient) {
    
  }
  getAll(){
    return this.http.get<JSONResponse>(this.url); 
     
  }
  
  
}
