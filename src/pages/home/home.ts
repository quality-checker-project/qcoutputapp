import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chart } from "chart.js";
import 'rxjs/add/operator/map';
import { HttpProvider } from '../../providers/http/http';
import { Defect } from '../../model/Defect';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild("barCanvas") barCanvas: ElementRef;
  private barChart: Chart;


  defectsArray: Defect[];
  outputArray = [];
  labelArray = [];
  totalCount: number;
  defectsCount: number;
  defectRatio: number;
  refreshRate=60000;
  constructor(public navCtrl: NavController, public httpProvider: HttpProvider) {


  }

  getArrayFromApi(defects: Defect[]) {
     this.outputArray=[];
     this.labelArray=[];
    defects.forEach(element => {
      this.outputArray.push(element.defectCount);
      this.labelArray.push(element.time);
    });
  }
  plotChart(defects:number[],label:string[]){
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",

      data: {
        labels: label,
        datasets: [
          {
            label: "Number of Defects",
            data: defects,
            backgroundColor: "rgba(255, 10, 10, 0.9)",
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        },
        layout: {
          padding: {
            left: 10,
            right: 10,
            top: 15,
            bottom: 15
          }
        }
      }
    });

  }
  getAndPlot() {
    this.httpProvider.getAll().subscribe(data => {
      if (data) {
        this.totalCount = data.totalData;
        this.defectsCount = data.totalDefects;
        this.defectRatio = data.defectRatio;
        if (data.defects) {
          this.getArrayFromApi(data.defects);
        }
        this.plotChart(this.outputArray,this.labelArray)
      }
    })
  }

ngOnInit(): void {
  this.getAndPlot();
  setInterval(this.getAndPlot. bind(this),this.refreshRate);
}




}
