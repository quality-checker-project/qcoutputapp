import { Defect } from "./Defect";

export class JSONResponse{
    totalData: number;
    totalDefects:number;
    defectRatio:number;
    defects: Defect[];

    constructor(totalData: number,totalDefects:number,defectsRatio:number, defects: Defect[]){
        this.totalData = totalData;
        this.totalDefects=totalDefects;
        this.defectRatio=defectsRatio;
        this.defects=defects;
    }
}